#!/usr/bin/env python3

import pygame
import random

import map
import loader
import main


GRASS_SPEED = 0.75
GRASS_GREEN = 7

CENTER_POS = (-1, -1)


def rotate_center(image, rect, angle):
    rotated_image = pygame.transform.rotate(image, angle)

    rotated_rect = rotated_image.get_rect(center=rect.center)

    return rotated_image, rotated_rect


def generate_spawn_point():

    x = random.randint(0, 9)
    y = random.randint(0, 9)

    while map.map_1[y][x] == 5:
        x = random.randint(0, 9)
        y = random.randint(0, 9)

    return x * 1000 + CENTER_POS[0], y * 1000 + CENTER_POS[1]


class Player(pygame.sprite.Sprite):

    def __init__(self):
        super(Player, self).__init__()

        self.image = loader.load_image('car_player.png')
        self.rect = self.image.get_rect()

        self.image_orig = self.image

        self.screen = pygame.display.get_surface()
        self.area = self.screen.get_rect()

        CENTER_POS = (main.DISPLAY_RESOLUTION[0]//2,
                      main.DISPLAY_RESOLUTION[1]//2)
        self.x = CENTER_POS[0]
        self.y = CENTER_POS[1]

        self.rect.topleft = self.x, self.y

        self.x, self.y = generate_spawn_point()

        self.dir = 0
        self.speed = 0

        self.max_speed = 11.5
        self.min_speed = -1.85
        self.acceleration = 0.095
        self.deacceleration = 0.12

        self.softening = 0.04

        self.steering = 1.6
        self.tracks = False
