#!/usr/bin/env python3

import pygame
import random
import array

import os
import sys


DISPLAY_RESOLUTION = (1024, 768)
DISPLAY_CENTER = (DISPLAY_RESOLUTION[0] // 2, DISPLAY_RESOLUTION[1] // 2)

TRAFFIC_COUNT = 45
CENTER_WIDTH = -1
CENTER_HEIGHT = -1


if __name__ == '__main__':
    pygame.init()

    clock = pygame.time.Clock()
    is_running = True
    main_font = pygame.font.Font('data/fonts/BRUN.TTF', 30)

    map_sprite = pygame.sprite.Group()
    player_sprite = pygame.sprite.Group()
    traffic_sprite = pygame.sprite.Group()
    tracks_sprite = pygame.sprite.Group()
    target_sprite = pygame.sprite.Group()
    pointer_sprite = pygame.sprite.Group()

    game_screen = pygame.display.set_mode(DISPLAY_RESOLUTION)
    pygame.display.set_caption('UnGTA')

    background = pygame.Surface(game_screen.get_size())

    background.fill((26, 26, 26))

    pygame.quit()
    sys.exit()
