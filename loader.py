#!/usr/bin/env python3

import pygame
import os


def load_image(in_file, in_transparent=True):
    full_path = os.path.join('data/imgs', in_file)

    image = pygame.image.load(full_path)

    if in_transparent:
        image = image.convert()
        color_key = image.get_at((0, 0))
        image.set_colorkey(color_key, pygame.RLEACCEL)
    else:
        image = image.convert_alpha()

    return image
